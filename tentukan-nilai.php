<?php
function tentukan_nilai($number)
{
    $hasil = "";
    if ($number >= 85 && $number <= 100) {
        $hasil = "Sangat Baik";
    } elseif ($number >= 70 && $number < 85) {
        $hasil = "Baik";
    } elseif ($number >= 60 && $number < 70) {
        $hasil = "Cukup";
    } else {
        $hasil = "Kurang";
    }
    return $hasil;
    echo "<br>";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>