<?php
require_once "Animal.php";

$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo $sheep->legs; // 2
echo $sheep->cold_blooded; // false
echo "<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

require_once "Ape.php";
// index.php
$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "<br>";

require_once "Frog.php";
$kodok = new Frog("buduk");
$kodok->jump(); // "hop hop"