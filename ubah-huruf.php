<?php
function ubah_huruf($string){
    $char = range('a', 'z');
    $huruf = "";
    foreach (str_split($string) as $str) {
        for ($i=0; $i < count($char); $i++) { 
            if ($str == $char[$i]) {
                $huruf .= $char[$i + 1];
            }
        }
    }
    return $huruf;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>